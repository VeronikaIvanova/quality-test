﻿using QualityTest.Good;
using QualityTest.Good.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualityTest.Bad.Impl
{
    public class Smartphone : IPhone, IVideo, IMusic, IReader, IMessageSender, IAlarmClock, ICamera
    {
        private SimplePhone phone;
        private List<String> videos = new List<string>();
        private List<String> photos = new List<string>();
        private List<String> phones = new List<string>();
        private List<String> freePhones = new List<string>();
        private List<String> messages = new List<string>();
        private List<DateTime> dates = new List<DateTime>();
        private List<String> songs = new List<string>();
        private string ownerName;
        private string ownerNumber;
        private int price;
        private int balance;
        private int memory;
        private List<String> books = new List<string>();


        public Smartphone(string ownerNumber, int price, int balance, int memory)
        {
            this.ownerNumber = ownerNumber;
            this.price = price;
            this.balance = balance;
            phone = new SimplePhone(ownerNumber, balance, price);
            this.memory = memory;
        }

        public void Call(string number)
        {
            try
            {
                if (!phones.Contains(number))
                {
                    phones.Add(number);
                }

                if (freePhones.Contains(number))
                {
                    CallFree(number);
                }
                else
                {
                    phone.Call(number);
                }

            } catch (Exception ex)
            {
      
            }
            
        }

        public void CallFree(string number)
        {
            try
            {
                if (freePhones.Contains(number))
                {
                    phone.Call(number);
                    phone.TopUpBalance(price);
                }

            }
            catch (Exception ex)
            {

            }

        }


        public string GetPhoto(int num)
        {
            if (photos.Count > num)
            {
                Console.WriteLine("photo number "+num);

                string photo = photos[num];

                return photo;

            }

            return "";
        }

        public string  TakePicture()
        {
            int num = photos.Count;
            bool found = true;
            if (num< memory)
            {
                while (found)
                {
                    found = false;
                    string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                    char[] stringChars = new char[8];
                    Random random = new Random();

                    for (int i = 0; i < stringChars.Length; i++)
                    {
                        stringChars[i] = chars[random.Next(chars.Length)];
                    }

                    var finalString = new String(stringChars);


                    foreach (String photo in photos)
                    {
                        if (photo.Equals(finalString))
                        {
                            found = true;
                            break;
                        }

                    }
                    photos.Add(finalString);
                    if (found)
                    {
                        Console.WriteLine("Photo with such key already exists, trying to generate a new one");
                    }
                    else
                    {
                        return finalString;
                    }
                }

            }


            return ""; 

        }

        public void Play(string song)
        {
            int length = songs.Count;
            List<int> toWatch = new List<int>();

            for (int i = 0; i < length; i++)
            {
                if (songs.Equals(songs))
                {
                    toWatch.Add(i);

                }
            }

            foreach (int i in toWatch)
            {
                Console.WriteLine("playing " + i + " song with name " + song);
                int score = 0;
                Random random = new Random();
                for (int r = 0; r < 10; r++)
                {
                    score += random.Next(0, 10);
                }

                Console.WriteLine("song " + i + " finished with  score " + score);

            }
        }

        public void Read(string book)
        {
            int length = books.Count;
            List<int> toWatch = new List<int>();

            for (int i = 0; i < length; i++)
            {
                if (book.Equals(book))
                {
                    toWatch.Add(i);

                }
            }

            foreach (int i in toWatch)
            {
                Console.WriteLine("reading " + i + " book with name " + book);
                int score = 0;
                Random random = new Random();
                for (int r = 0; r < 10; r++)
                {
                    score += random.Next(0, 10);
                }

                Console.WriteLine("book " + i + " finished with  score " + score);

            }
        }

        public void Ring(DateTime date)
        {
           if (!dates.Contains(date))
            {
                dates.Add(date);
            }

            while (true)
            {
                if (System.DateTime.Now.Equals(date))
                {

                    Console.WriteLine("Alarm!");

                    Random rnd = new Random();
                    int x = 0;
                    if (x% 2 == 0)
                    {
                        date.AddDays(1);
                    }
                    else
                    {
                        break;
                    }
                        
                }
            }
            
        }

        public bool Send(string text, string number)
        {
            if (phones.Contains(number))
            {
                phones.Add(number);
            }
            messages.Add(text);


            return true;
           

        }




        public void Watch(string video)
        {

            int length = videos.Count;
            List<int> toWatch = new List<int>();

            for(int i=0; i< length; i++)
            {
                if (video.Equals(video))
                {
                    toWatch.Add(i);

                }
            }

            foreach (int i in toWatch)
            {
                Console.WriteLine("playing " + i + " video with name " + video);
                int playing = 0;
                Random random = new Random();
                int score = 0;
                for (int r=0; r< 10; r++)
                {
                    score += random.Next(0, 10);
                }

                Console.WriteLine("video " + i + " finished with  score "+ score);

            }

        }
    }
}
