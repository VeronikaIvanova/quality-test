﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualityTest.Bad
{
    public interface IMessageSender
    {
       bool Send(string text, string number);
    }
}
