﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualityTest.Good.Impl
{
    public class SimplePhone : IPhone
    {
        private string ownerNumber;
        public int balance;
        private int price;

        public SimplePhone(string ownerNumber, int balance, int price)
        {
            this.ownerNumber = ownerNumber;
            this.balance = balance;
            this.price = price;

        }

        public void Call(string number)
        {
           if (number.Equals(ownerNumber))
            {
                throw new Exception("Can't call yourself");
            }
           if (balance - price< 0)
            {
                throw new Exception("Top up balance");
            }
            Console.WriteLine("Calling to " + number);
            balance -= price;

        }

        public void TopUpBalance(int sum)
        {
            this.balance += sum;
        }

    }
}
